import math
from sys import float_info
import pandas as pd
from plotnine import ggplot, aes, geom_point, scale_x_log10, scale_y_log10,\
    facet_grid, geom_vline, labs, theme
from itertools import repeat


def u(x, rownanie):
    return rownanie["funkcja"](x) / rownanie["pochodna"](x)


def wielepunktow(x, rownanie):
    f = rownanie["funkcja"]
    p = rownanie["pochodna"]
    mianownik = p(x - 2 * f(x) / (3 * p(x - u(x, rownanie) / 3)))

    return 0.25 * (u(x, rownanie) + 3 * f(x) / mianownik)


def znajdz_rozwiazanie(x, eps, metoda, rownanie):
    k = 0
    h = eps
    kroki = []
    dziel_zero = False
    zbiezne = True
    poprzednie_x = (None, None)

    while abs(h) >= eps and k < 1000:
        try:
            h = -metoda(x, rownanie)
        except ZeroDivisionError:
            dziel_zero = True
            break

        if poprzednie_x[1] == x:
            zbiezne = False
            break

        poprzednie_x = x, poprzednie_x[0]
        x += h
        kroki.append(x)
        k += 1

    return k, kroki, dziel_zero, zbiezne


def wartosc_oczekiwana(x, wartosci_dokladne):
    roznice = map(lambda i: abs(i - x), wartosci_dokladne)
    minimum = float('inf')
    xp = float('nan')

    for roznica, wartosc_dokladna in zip(roznice, wartosci_dokladne):
        if roznica < minimum:
            minimum = roznica
            xp = wartosc_dokladna

    return xp


def znajdz_blad(x, xp):
    dzielnik = 1 if xp == 0 else xp
    return abs((x - xp) / dzielnik)


def wykonaj_testy(metody, rownania, testy):
    wartosci = []

    for rownanie in rownania:
        for metoda in metody:
            for test in testy:
                for dokladnosc in test['dokladnosci']:
                    for punkt in test['punkty']:
                        k, kroki, dziel_zero, zbiezne =\
                            znajdz_rozwiazanie(punkt, dokladnosc,
                                               metoda['funkcja'], rownanie)

                        xp = wartosc_oczekiwana(kroki[-1],
                                                rownanie['wartosci_dokladne'])

                        blad = znajdz_blad(kroki[-1], xp)

                        wartosci.append((kroki, k, dokladnosc, punkt, blad, xp,
                                         dziel_zero, zbiezne, metoda['nazwa'],
                                         rownanie['nazwa'], test['nazwa']))

    return pd.DataFrame(wartosci, columns=['kroki', 'k', 'eps', 'x0', 'błąd',
                                           'xp', '/0', 'zbieżność', 'metoda',
                                           'równanie', 'test'])


def obrob_dane(df):
    kolumna = ['Z'] * len(df)

    for i in range(len(df)):
        if df['/0'][i]:
            kolumna[i] = '/0'
        elif ~df['zbieżność'][i]:
            kolumna[i] = 'NZ'

    df = df.assign(uwagi=kolumna,
                   błąd_względny=['Błąd bezwzględny' if x == 0 else
                                  'Błąd względny' for x in df['xp']])
    return df


def wypiszAC(df):
    df = df.sort_values(by=['x0'], kind='mergesort')
    punkt = None

    for _, wiersz in df.iterrows():
        if punkt != wiersz['x0']:
            print(f"\nDla punktu początkowego x0 = {wiersz['x0']}:")
            punkt = wiersz['x0']

        print(f"x = {wiersz['kroki'][-1]: .10e}, eps = {wiersz['eps']:.2e},",
              f"k = {wiersz['k']:3}, {'Δ' if wiersz['xp'] == 0 else 'δ'}(x) "
              f"= {wiersz['błąd']:e} {wiersz['uwagi']}")


def wypiszBD(df):
    dokladnosc = None

    for _, wiersz in df.iterrows():
        if dokladnosc != wiersz['eps']:
            print(f"\nDla dokładności epsilon = {wiersz['eps']}:")
            dokladnosc = wiersz['eps']

        print(f"x = {wiersz['kroki'][-1]: .10e}, x0 = {wiersz['x0']:4},",
              f"k = {wiersz['k']:3}, {'Δ' if wiersz['xp'] == 0 else 'δ'}(x) "
              f"= {wiersz['błąd']:e} {wiersz['uwagi']}")


def wypisz_uwagi(df, komunikat):
    print("\n\n" + "*" * 75 + "\n\n" + komunikat)

    for _, wiersz in df.iterrows():
        print(f"x0 = {wiersz['x0']:4}, eps = {wiersz['eps']:.2e}, "
              f"{wiersz['metoda']:20} równanie: {wiersz['równanie']}.")
        print(f"Ostatnie kroki: {wiersz['kroki'][-3:]}")


def wypisz_zbieznosci(df):
    df = df[df['uwagi'] == 'Z'].sort_values(by=['k'], ascending=False)
    dokladnosc = min(df['eps'].tolist())

    print("\n\n" + "*" * 75 + "\n")
    print("Ciągi błędów względnych (bezwzględnych dla pierwiastka = 0).")

    for pierwiastek in set(df['xp'].tolist()):
        for metoda in set(df['metoda'].tolist()):
            wiersz = df.loc[(df['eps'] == dokladnosc)
                            & (df['xp'] == pierwiastek)
                            & (df['metoda'] == metoda)].iloc[0]

            print(f"\nPierwiastek: {pierwiastek}, {metoda}, "
                  f"równanie: {wiersz['równanie']} x0 = {wiersz['x0']}.")

            for i in map(znajdz_blad, wiersz['kroki'], repeat(wiersz['xp'])):
                print(f"{i:.8e}")


def wypisz(df, metody, rownania, testy):
    for test in testy:
        for rownanie in rownania:
            print("=" * 75)
            print(f"{rownanie['nazwa']}:")

            for metoda in metody:
                print("-" * 75)
                print(f"{metoda['nazwa']}:")

                test['wypisz'](df.loc[(df['metoda'] == metoda['nazwa'])
                                      & (df['równanie'] == rownanie['nazwa'])
                                      & (df['test'] == test['nazwa'])])

    wypisz_uwagi(df[~df['zbieżność']], 'Brak zbieżności dla:')
    wypisz_uwagi(df[df['/0']], 'Dzielenie przez zero wystąpiło dla:')
    wypisz_zbieznosci(df)


def rysuj_wykres(df, os_x, os_y, kolor, tytul='', podzial='metoda~równanie'):
    if os_y == "błąd":  # Niezbieżne mają błędy rzędu jedności.
        df = df[df['zbieżność']]  # Nie widać na wykresach nic innego.

    wykres = (
            ggplot(df)
            + aes(x=os_x, y=os_y, shape=f'factor({kolor})',
                  fill=f'factor(uwagi)')
            + geom_point(size=4)
            + labs(title=tytul)
            + facet_grid(facets=podzial)
            + theme(legend_position='left')
    )

    if os_y == 'błąd':
        wykres += scale_y_log10()

    if os_x == "eps":
        wykres += scale_x_log10()
    else:
        wykres += geom_vline(df, aes(xintercept='xp'))

    print(wykres)


def utworz_wykresy(df):
    rysuj_wykres(df[df['test'] == 'AC'], 'eps', 'k', 'x0', 'Test A')
    rysuj_wykres(df[df['test'] == 'BD'], 'x0', 'k', 'eps', 'Test B')
    rysuj_wykres(df[df['test'] == 'AC'], 'eps', 'błąd', 'x0', 'Test C',
                 'metoda~równanie+błąd_względny')
    rysuj_wykres(df[df['test'] == 'BD'], 'x0', 'błąd', 'eps', 'Test D',
                 'metoda~równanie+błąd_względny')


def projekt(metody, rownania, testy):
    df = obrob_dane(wykonaj_testy(metody, rownania, testy))
    wypisz(df, metody, rownania, testy)
    utworz_wykresy(df)


if __name__ == "__main__":
    testowane_metody = (
        {"funkcja": u,
         "nazwa": "Metoda Newtona"},
        {"funkcja": wielepunktow,
         "nazwa": "Metoda wielopunktowa"}
    )

    testowane_rownania = (
        {"funkcja": lambda x: 2 * x - math.cos(x) - math.pi,
         "pochodna": lambda x: 2 + math.sin(x),
         "wartosci_dokladne": (math.pi / 2,),
         "nazwa": "2x - cos(x) - PI"},
        {"funkcja": lambda x: x * (2 * x + 3.1) * (2 * x + 3.1),
         "pochodna": lambda x: 12*x*x + 24.8*x + 9.61,
         "wartosci_dokladne": (-1.55, 0),
         "nazwa": "x(2x + 3.1)^2"}
    )

    parametry_testow = (
        {"punkty": (-100, -5, 5, 100),
         "dokladnosci": (1E-2, 1E-4, 1E-6, 1E-8, 1E-10, 1E-12,
                         1E-14, 1E-15, float_info.epsilon),
         "nazwa": "AC",
         "wypisz": wypiszAC},
        {"punkty": (-100, -80, -60, -40, -20, -10, -5, -3, -1,
                    1, 3, 5, 10, 20, 40, 60, 80, 100),
         "dokladnosci": (1E-5, 1E-11, float_info.epsilon),
         "nazwa": "BD",
         "wypisz": wypiszBD}
    )

    projekt(testowane_metody, testowane_rownania, parametry_testow)
