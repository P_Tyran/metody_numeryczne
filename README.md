### Porównanie działania dwóch metod rozwiązywania równań nieliniowych na wybranych równaniach ###

* Porównywane metody:
* Metoda Newtona:

![Alt text](https://tinypic.host/images/2022/03/13/1.png)

* Metoda wielopunktowa:

![Alt text](https://tinypic.host/images/2022/03/13/2.png)

* Wykorzystane równania:

![Alt text](https://tinypic.host/images/2022/03/13/3.png)
